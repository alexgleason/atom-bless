blessjs = require './blessjs'
fs = require 'fs'
path = require 'path'
{CompositeDisposable} = require 'atom'

module.exports = Bless =
  blessView: null
  modalPanel: null
  subscriptions: null

  activate: (state) ->

    # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    @subscriptions = new CompositeDisposable

    # Register command that toggles this view
    @subscriptions.add atom.commands.add 'atom-workspace', 'bless:bless': => @bless()

  deactivate: ->
    @subscriptions.dispose()

  serialize: ->

  bless: (input) ->
    blessjs.bless atom.workspace.getActivePaneItem().getPath()
