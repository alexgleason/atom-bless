/*
Copyright (c) 2013 Paul Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

var input = '';
var output = input;

var path = require('path'),
    fs = require('fs'),
    util = require('util'),
    console = require('console');

var bless = require('bless');

var options = {
    cacheBuster: true,
    cleanup: true,
    compress: false,
    force: true,
    imports: true
};

function noun(noun, variable) {
    if (variable != 1) {
        noun += 's';
    }
    return noun;
}

function formatNumber (nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';

    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }

    return x1 + x2;
}


var parseCss = function (e, data) {
    if (e) {
        atom.notifications.addFatalError('Bless', { detail: e.message });
        process.exit(1);
    }

    new (bless.Parser)({
        output: output,
        options: options
    }).parse(data, function (err, files, numSelectors) {
        if (err) {
            throw err;
            process.exit(1);
        } else {
            try {
                var selectorNoun = noun('selector', numSelectors);
                numSelectors = formatNumber(numSelectors);
                var message = 'Source CSS file contained ' + numSelectors + ' ' + selectorNoun + '.',
                    numFiles = files.length,
                    fileNoun = noun('file', numFiles);

                if(numFiles > 1 || input != output) {
                    for (var i in files) {
                        var file = files[i],
                            fd = fs.openSync(file.filename, 'w');
                        fs.writeSync(fd, file.content, 0, 'utf8');
                    }

                    message += ' ' + numFiles + ' CSS ' + fileNoun + ' created.';
                } else {
                    message += ' No changes made.';
                }


                bless.Parser.cleanup(options, output, function(err, files) {
                    if (err) {
                        throw err;
                        process.exit(1);
                    } else {
                        var oldVerb;

                        for (var i in files) {
                            var file = files[i];

                            if(! options.cleanup) {
                                oldVerb = 'renamed';
                                var ext = path.extname(file),
                                    dest = file.replace(ext, '-old' + ext),
                                    read = fs.createReadStream(file),
                                    write = fs.createWriteStream(dest);

                                read.on('end', function (err) {
                                    if (err) {
                                        throw err;
                                        process.exit(1);
                                    }
                                });

                                util.pump(read, write);
                            } else {
                                oldVerb = 'removed';
                            }

                            fs.unlink(files[i], function (err) {
                                if (err) {
                                    throw err;
                                    process.exit(1);
                                }
                            });
                        }

                        var numOld = files.length;

                        if (numOld > 0) {
                            var removedFileNoun = noun('file', numOld);
                            message += ' Additional CSS ' + removedFileNoun + ' no longer needed. ' + numOld + ' additional ' + removedFileNoun + ' ' + oldVerb + '.';
                        }

                        atom.notifications.addSuccess('Bless', { detail: message });
                    }
                });

            } catch (e) {
                throw e;
                process.exit(2);
            }
        }
    });
};

exports.bless = function (inputFile) {
    input = inputFile;
    output = input;
    fs.readFile(input, 'utf-8', parseCss);
}
